# name: get youtube playlist
# start: 17 jan 2022
# versions: 0.1 beta
# last update: 17 jan 2022
# by: VRTK  ( ksemvis@gmail.com )
#
#
# download python
#   windows : https://www.python.org/ftp/python/3.10.1/python-3.10.1-amd64.exe
#   mac :
#   linux :
#
# download geckodriver
#   URL : https://github.com/mozilla/geckodriver/releases
#   windows file : https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-win64.zip
#
#
# :: python library setup
# pip install selenium
# pip install webdriver-manager
#
from selenium import webdriver
from selenium.webdriver.firefox.service import Service
# from webdriver_manager.firefox import GeckoDriverManager
# from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
from time import sleep
import os

# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC

# Global config
FIREFOX_PATH = os.path.abspath(os.getcwd()) + r'\geckodriver.exe'
YOUTUBE_URL = 'https://www.youtube.com/watch?v=irNfyAVkKFg&list=PLjPfp4Ph3gBraIRrw36cU2w7XkoRSo7w9&index=1'
XPATH_PLAYLIST = '/html/body/ytd-app/div/ytd-page-manager/ytd-watch-flexy/div[5]/div[2]/div/ytd-playlist-panel-renderer/div/div[2]/ytd-playlist-panel-video-renderer'


YOUTUBE_URL = input("input Youtube playlist..")
print("firefox starting...")
services = Service(FIREFOX_PATH)
options = webdriver.FirefoxOptions()
driver = webdriver.Firefox(service=services, options=options)
driver.implicitly_wait(20)

driver.get(YOUTUBE_URL)
playlist = driver.find_elements(By.XPATH, XPATH_PLAYLIST)

i = 0
while i < len(playlist):
    # print(str(i) + ". " + playlist[i].text)
    # print(str(i) + ". " + playlist[i].text.split('\n')[2] )
    direct_URL = playlist[i].find_elements(
        By.XPATH, r'//*[@id="wc-endpoint"]')[i].get_attribute('href')
    print(direct_URL)
    i += 1
